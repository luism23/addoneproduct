<?php
/*
Plugin Name: AddOneProduct
Plugin URI: https://gitlab.com/luism23/addoneproduct
Description: AddOneProduct
Author: LuisMiguelNarvaez
Version: 1.0.1
Author URI: https://gitlab.com/luism23
License: GPL
 */


add_filter( 'woocommerce_add_cart_item_data', 'addOneProduct_4541621652', 10, 1 );

function addOneProduct_4541621652( $cartItemData ) {
	wc_empty_cart();

	return $cartItemData;
}
